import React from 'react';
// Styles
import s from './approveModal.module.scss';
// Utils
import { Modal } from 'react-bootstrap';
import Button from '../Button';

interface IModalProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  show?: boolean;
  name?: string;
  handleClose: () => void;
  handleSubmit: () => void;
}

const ApproveModal: React.FC<IModalProps> = ({
	show = true,
	name,
	handleClose,
	handleSubmit
}) => {
	return (


		<Modal animation={false} className={s.root} contentClassName={s.content} show={show} onHide={handleClose}>
			<Modal.Body className={s.modalBody}>
				<div className={s.container}>
					<span className={s.title}>You have selected to approve this document</span>
					<span className={s.pageTitle}>{name}</span>
				</div>
				<div className={s.footer}>
					<Button className={s.button} color="success"  onClick={handleSubmit}>
						Confirm
					</Button>
					<Button className={s.button} color="primary" variant='outline' onClick={handleClose}>
						Cancel
					</Button>
				</div>
			</Modal.Body>

		</Modal>
	);
};

export default ApproveModal;
